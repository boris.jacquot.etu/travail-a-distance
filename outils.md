Ce document présente de façon synthétique un ensemble d'outils utiles
liés à l'enseignement à distance à Polytech. Les Systèmes d'Exploitation
(OS) concernés sont précisés entre paranthèse pour chaque outil.

### Table des matières

[Transférer des fichiers](#transferer-des-fichiers)

[Partager des fichiers](#partager-des-fichiers)

[Utiliser Linux chez soi](#utiliser-linux-chez-soi)

[Utiliser les logiciels de Polytech](#utiliser-les-logiciels-de-polytech)

[Editeurs de texte non graphiques](#editeurs-de-texte-non-graphiques)

# Transférer des fichiers
Cette section présente plusieurs outils vous permettant de transférer
des fichiers entre les machines de Polytech et votre ordinateur
personnel.

## FileZilla (multi)
Cet outil possède une interface graphique similaire aux outils
classiques d'exploration de fichiers Windows/Linux.

Installation :

* Windows : télécharger depuis le [Site
  web](https://filezilla-project.org/)

* Linux : disponible via les gestionnaires de paquet.
  Ex : `sudo apt install filezilla`

Configuration : Menu `Fichier` -> `Gestionnaire de site` ->

* Hôte : portier.polytech-lille.fr

* Port : 2222

* Protocole : SFTP


## Winscp (Windows)

Outil similaire à FileZilla, réservé à Windows.

Installation :
Depuis le [site](https://winscp.net/).

Configuration : voir la configuration de FileZilla.

## scp (Linux)

Commande utilisable depuis votre terminal Linux. Ci-dessous quelques
exemples d'utilisation classique. Comme d'habitude, pour plus
d'informations se référer au `man scp`.

* De Polytech à mon PC :
`scp -P 2222 jforget@portier.polytech-lille.fr:/tmp/test/fichier.txt .`

* Pour un répertoire :
`scp -P 2222 -r jforget@portier.polytech-lille.fr:/tmp/test/ .`

* De mon PC vers Polytech :
`scp -P 2222 toto.txt jforget@portier.polytech-lille.fr:/tmp/test/`

## sshfs (Linux)

Cet outil est un système de gestion de fichier capable d'opérer sur des
fichiers distants à travers une connexion `ssh`. L'outil n'est pas
installé par défaut, utiliser votre gestionnaire de paquet pour
l'installer (e.g. `sudo apt install sshfs`).

Concrètement, une fois la commande lancée, cela permet d'opérer sur des
fichiers distants exactement comme s'ils étaient disponibles localement
sur votre machine. Exemple d'utilisation :

1. `mkdir AccueilRep`
2. `sshfs -p 2222 -r jforget@portier.polytech-lille.fr:/home/gisEns/jforget AccueilRep`
3. Les fichiers de `jforget` sont dispos dans `AccueilRep`. Toute
   modification effectuée dans `AccueilRep` affecte le contenu distant
4. `umount AccueilRep` permet d'arrêter la connexion distante.

# Partager des fichiers

Cette section présente des outils permettant de partager des fichiers
entre plusieurs utilisateurs, qui peuvent être connectés sur des
machines ou réseaux distincts.

## git (multi)

Git est beaucoup plus qu'un utilitaire de partage de fichiers. C'est
un outil de gestion de versions, référence pour le développement
collaboratif. 

* [Un cours d'introduction](https://rudametw.github.io/teaching/#topOfPage)

* [Le gitlab de l'université](https://gitlab.univ-lille.fr/)

## nextcloud (web)

Nextcloud est un outil d'hébergement de fichiers en ligne. Il est
initialement plutôt destiné à la sauvegarde de fichiers de grande
taille.

* Votre espace de stockage à l'université est
  [ici](https://nextcloud.univ-lille.fr) ;
  
* L'interface propose plusieurs options de partage de
  fichiers/répertoires. Il est en particulier possible de préciser 
  les personnes avec qui partager à partir de leur adresse email.

## Moodle (web)

Moodle est un outil de gestion de cours à distance. Pour accéder aux
ressources d'un cours, il faut en général commencer par vous y inscrire
(demandez à l'enseignant concerné). Quelques fonctionnalités notoires :

* Un Wiki, dans lequel vos enseignants déposent les ressources de leurs
  cours ;
  
* La possibilité pour les étudiants de déposer des fichiers pour
  répondre aux demandes de leurs enseignants ;
  
* Des outils de forums, liés aux cours.

Le Moodle de l'université est [ici](https://moodle.univ-lille.fr/).

# Utiliser Linux chez soi

Il existe différentes solutions pour travailler dans un environnement
Linux depuis chez vous.

## Installer Linux sur son PC

En préliminaire, sachez que vous avez deux configurations possibles :

* Installer Linux comme unique OS sur votre machine ;
* Installer Linux en *dual-boot*, c'est-à-dire côte-à-côte avec un
système Windows. Vous pourrez ensuite choisir à chaque lancement de
votre PC entre les 2 OS. Ceci nécessite cependant de *partitionner*
votre disque dur, cf section ci-desous.


Vous pouvez télécharger gratuitement Linux sur le web. Il existe de
nombreuses déclinaisons, appelées *distributions*. Les plus répandues :

* [Ubuntu](https://ubuntu.com/download) : réputée pour son style
  "user-friendly" ;

* [Debian](https://www.debian.org/distrib/) : réputée auprès des adeptes
  authentiques du logiciel libre. Donne plus de pouvoir à l'utilisateur,
  donc aussi plus de responsabilités.
  
Les sites indiqués ci-dessus vous expliquent comment faire pour booter à
partir d'une clé USB sur laquelle vous copiez les fichiers
téléchargés. Petite complication, il faut pour cela aller modifier le
BIOS :

1. Pour modifier le BIOS, utiliser le raccourci clavier correspondant,
   dès le démarrage de votre machine ;
2. Ce raccourci dépend de votre constructeur, se renseigner sur le web ;
3. L'organisation de votre BIOS varie là aussi selon les
   constructeurs. Vous devez cherchez une section du style `Boot
   sequence`, `Boot order`, `Boot Device Options`, ...
4. Une fois le boot depuis USB activé, redémarrer votre machine.

## Partitionner votre disque dur

Partitionner un disque consiste à le séparer en plusieurs régions, de
façon à ce que chacune puisse être gérée séparément. Dans notre cas,
cela permet d'installer un OS différent sur chaque partition. Il existe
de nombreux outils de partitionnement, par exemple :

* **gparted** : fait partie de l'environnement graphique GNOME de
  Linux. Il s'agit d'un outil graphique (lancer depuis le termina, qui
  vous permet de créer, supprimer, ou redimensionner des partitions. On
  peut ainsi redimensionner une partition Windows existante pour
  installer Linux à côté, sans "casser" Windows.:
  * Vous pouvez y accéder en bootant depuis votre clé USB Linux,
  puis en "l'installant" temporairement en mémoire : `sudo apt install
  gparted` ;
  * Lancer depuis le terminal : `gparted &` ;
  * Sachez que les opérations de partitionnement peuvent s'avérer relativement
    longues.

## Linux sous Windows

Si vous souhaitez utiliser les outils Linux depuis Windows, il existe
plusieurs alternatives.

### Windows Subsystem for Linux, WSL (Windows 10)

WSL est une *couche de compatibilité* permettant d'exécuter des
exécutables binaires Linux sous Windows 10.

* Installation expliquée
  [ici](https://docs.microsoft.com/en-us/windows/wsl/install-win10)
  
* Une fois installé, WSL se présente comme un terminal Linux classique.

### ssh

Une autre solution consiste à utiliser les outils `ssh` pour ouvrir un
shell distant sur une machine Linux, par exemple une machine de
Polytech. Voir la section dédiée à [ssh](#ssh) pour plus de
détails.

### cygwin (Windows < 10)

Cygwin est une collection d'outils fournissant des fonctionnalités
similaires aux outils Linux. Contrairement à WSL, *il ne permet pas
d'exécuter directement des binaires Linux*. 

* [Site web](https://www.cygwin.com/)
* Plus ancien que WSL, il présente cependant l'avantage de fonctionner
  sur toute installation Windows.

### Virtualbox (multi)

Virtualbox est un *hyperviseur*, c'est-à-dire qu'il permet de faire
fonctionner un ou plusieurs sous-OS à l'intérieur d'un OS existant. Plus
lourd, mais aussi plus flexible qu'un dual-boot.

* [Site web](https://www.virtualbox.org/wiki/Downloads)

# Utiliser les logiciels de Polytech
## ssh (multi)
Cette commande permet de lancer un Shell (terminal) sur une machine
distante. La connexion est sécurisée (les messages échangés sont
cryptés). Ceci vous permet donc de vous connecter sur les machines de
Polytech, et ainsi d'accéder aux logiciels installés sur ces machines.

Ci-dessous, un exemple d'utilisation classique. Comme d'habitude,
pour plus d'informations se référer au `man ssh`. Attention, les options
sont très proches de celles de `scp` mais diffèrent légèrement.

* Pour entrer à Polytech :
`ssh -l jforget -p 2222 portier.polytech-lille.fr`
* Se connecter ensuite sur une machine de TP pour avoir accès aux
logiciels habituels : `ssh machine_TP` en ramplaçant `machine_TP` par
votre machine de TP préférée.

Pour connaître les machines de TP actuellement allumées vous pouvez
utiliser la commande ci-dessous, proposée par Xavier Redon :

```shell
for machine in gambrinus lyderic gayant reuze bimberlot phinaert clodion florine astruc gedeon ; do  
  for i in 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 ; do  
      ping -c1 -W1 $machine$i 2> /dev/null > /dev/null && echo $machine$i is alive  
  done  
done  
```

**NB** : Ce script peut prendre assez longtemps à s'exécuter. Vous pouvez
supprimer une partie des salles si besoin, par exemple `clodion`,
`florine`, `astruc`, `gedeon` semblent peu souvent allumées.

### ssh sous Windows

Un client `openssh` est désormais installé par défaut sur les systèmes
Windows 10. Pour l'utiliser, deux possibilités :

* Par la barre de recherche, lancez PowerShell. Dans PowerShell, tappez
  toute la commande `ssh` indiquée précédemment. ;
* Appuyez sur "Toûche Windows"+R, puis tappez toute la commande `ssh` indiquée
  précédemment.

Si vous avez Windows 10, mais que la commande `ssh` n'est pas
installée :

1. Barre de recherche
2. *Applications*
3. *Gérer les fonctionnalités facultatives*
4. *Ajouter une fonctionnalité*
5. *OpenSSH Client*

## putty (windows < 10)

Si vous disposez d'une version antérieure à Windows 10, vous pouvez
utiliser l'outil `putty` disponible [ici](https://www.putty.org/). Pour les
paramètres de connexion, voir la section sur [ssh](#ssh).

# Editeurs de texte non graphiques

Si vous optez pour une connexion à distance sur les machines de
Polytech, vous constaterez que l'absence d'interface graphique implique
le problème suivant : comment éditer un texte sans fenêtres, menus, ou
souris ? Heureusement, il existe plusieurs éditeurs de texte capables de fonctionner
sans interface.

## nano

C'est le plus primitif mais aussi le plus simple.

* Lancement : `nano toto.c`

* Raccourcis clavier : indiqués en base de l'écran. Le `^` signifie
  `Ctrl`, le `M` signifie `Alt`. Par exemple, `Ctrl+R` pour sauver,
  `Ctrl+X` pour quitter.

## emacs

Beaucoup plus riche que `nano`, ses capacités son tentaculaires
(compilation, mailer, explorateur web, ...). Il permet de travailler de
manière extrêmement efficace, à condition de prendre le temps de se
familiariser avec ses raccourcis clavier. Un bon point de départ :

* [Emacs Reference Card](https://www.gnu.org/software/emacs/refcards/pdf/refcard.pdf) :
  les raccourcis essentiels, en une page recto-verso.

## vi

`vi` et `emacs` sont les deux éditeurs de texte majeurs de Linux. Il
semble que les capacités soient comparables, question difficile à
trancher car quasiment personne ne prend ne le temps d'apprendre les
deux outils. Les raccourcis sont très différents de ce à quoi vous êtes
probablement habitué.

S'il vous arrive de le lancer par erreur, quitter avec le raccourci `Echap : q`

# Faire des captures d'écran

## import (Linux)

Ex : `import screener.png`
