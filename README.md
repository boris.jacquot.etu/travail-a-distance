# travail-a-distance

Ce dépôt git a pour objectif de centraliser un ensemble ressources
destinées à l'utilisation de divers outils d'enseignement à
distance, à Polytech'Lille :

* `outils.md` une brève synthèse des outils notoires pour le travail à
  distance.
